<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Uploading</h2>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">click </button>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Please insert field </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group" height="600px">
                        <label for="usr"> Descriptions:</label>
                        <input type="text" class="form-control" id="usr" placeholder="Describing about image">
                    </div>

                    <div class="date">
                        <label>
                            <input type="date"> select date
                        </label>
                    </div>

                    <div class="form-group" action="show.php" method="post" enctype="multipart/form-data" >
                        <label class="control-label">upload file</label>
                        <input type="file" class="filestyle" data-icon="false">
                    </div>

                </div>
                <div class="modal-footer">
                    <button onclick="location.href='show.php'" type="button" class="btn btn-default" data-dismiss="modal" name="submit">Send</button>
                </div>

            </div>
        </div>

    </div>

</body>
</html>